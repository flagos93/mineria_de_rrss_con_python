import pandas as pd
import numpy as np

df1 = pd.DataFrame(np.array([
    ["Maria",18,"Economia","maria@gmail.com"],
    ["Luis",22,"Medicina","luis@yahoo.es"],
    ["Carmen",20,"Arquitectura","carmen@gmail.com"],
    ["Antonio",21,"Economia","antonio@gmail.com"]]),
    columns=['Nombre','Edad','Grado','Correo'])
df1.index = df1.index+1

print (df1)
