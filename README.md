# Material de curso "Mineria de redes sociales con Python".

## Tarea 1: Dataframe en pandas.
    
    Bibliotecas utilizadas:
        pandas
        numpy
    
    pip3 install pandas
    pip3 install numpy
    python3 tarea1.py

## Tarea 2: Lectura y limpieza de DataFrame desde web.

    Bibliotecas utilizadas:
        pandas

    pip3 install pandas
    python3 Tarea2/src/tarea2.py
