import pandas as pd

#Lectura y asignacion de tabla desde web a DF
url = 'https://datosmacro.expansion.com/otros/coronavirus'
df_list = pd.read_html(url)
df_base = df_list[0]

#Limpieza columnas requeridas y formato de numeros
df_paisesyconfirmados = df_base[['Países','Confirmados']]
df_paisesyconfirmados['Confirmados'] = df_paisesyconfirmados['Confirmados'].str.replace('.','')
df_paisesyconfirmados.Confirmados = pd.Series(df_paisesyconfirmados['Confirmados'], dtype='int32')

#Filtro y busqueda de valores mayores y menores
df_10mayores = df_paisesyconfirmados.sort_values(by='Confirmados',ascending=False).head(10)
df_10menores = df_paisesyconfirmados.sort_values(by='Confirmados',ascending=True).head(10)

df_10mayores.to_csv('../output/mayores.csv',index=False,encoding='utf-8-sig')
df_10menores.to_csv('../output/menores.csv',index=False,encoding='utf-8-sig')
